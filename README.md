# URL Shortner

You can shorten an URL. 

Use online version: https://opencode-url-shortner.glitch.me


# Installation
Make sure you have Python installed.

```
git clone https://gitlab.com/boyuan12/url-shortner.git
cd url-shortner
pip3 install -r requirements.txt
export FLASK_APP=server.py
flask run
```

# API
No need to create an account! Just go to https://open-code-url-shortner.glitch.me/api?url=YOUR-URL-HERE. If success, you will get JSON response with newURL. Else an error.