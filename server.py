from flask import Flask, render_template, request, flash, redirect, jsonify
import sqlite3
import random
import string

app = Flask(__name__)
app.config["SECRET_KEY"] = "secret"

BASE_URL = "https://opencode-url-shortner.glitch.me/url"
conn = sqlite3.connect("db.sqlite3", check_same_thread=False)
c = conn.cursor()

try:
  c.execute("""CREATE TABLE "urls" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"original_url"	TEXT NOT NULL,
	"code"	TEXT NOT NULL,
	"auto_code"	TEXT
);""")

except:
  pass

def random_string():
    possible_char = string.ascii_letters + string.digits
    answer = ''
    for i in range(7):
        answer += random.choice(possible_char)
    return answer

def url_validator(url):
  import re
  regex = re.compile(
          r'^(?:http|ftp)s?://' # http:// or https://
          r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
          r'localhost|' #localhost...
          r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
          r'(?::\d+)?' # optional port
          r'(?:/?|[/?]\S+)$', re.IGNORECASE)

  return (re.match(regex, url) is not None)

@app.route("/", methods=["GET", "POST"])
def index():

    if request.method == "POST":

        if not request.form.get("url"):
            flash("Please provide all requried fields", category="danger")
            return redirect("/")

        code = random_string()

        codes = c.execute("SELECT code FROM urls").fetchall()

        while (code,) in codes:
            code = random_string()

        c.execute("INSERT INTO urls (auto_code, original_url, code) VALUES (:code, :url, :code)", {"url": request.form.get("url"), "code": code})

        conn.commit()

        return render_template("confirm.html", BASE_URL=BASE_URL, code=code, url=request.form.get("url"))

    else:
        return render_template("index.html")


@app.route("/update", methods=["POST"])
def update():

    result = c.execute("SELECT original_url FROM urls WHERE code=:code", {"code": request.form.get("code")}).fetchall()

    if len(result) == 0:
        flash("Invalid URL", category="danger")
        return redirect("/")

    codes = c.execute("SELECT code, auto_code FROM urls").fetchall()

    for code in codes:

        if request.form.get("new") == code[1]:
            break

        if request.form.get('new') == code[0]:
            flash("Already used", category="danger")
            return render_template("confirm.html", BASE_URL=BASE_URL, code=request.form.get("new"), url=result[0][0])

    c.execute("UPDATE urls SET code=:code WHERE code=:old_code", {"code": request.form.get('new'), "old_code": request.form.get("code")})

    conn.commit()

    flash("Now you can go to {}/{}".format(BASE_URL, request.form.get('new')), category="success")
    return redirect("/")


@app.route("/url/<string:code>")
def redirect_url(code):

    result = c.execute("SELECT original_url FROM urls WHERE code=:code", {"code": code}).fetchall()

    if len(result) == 0:
        flash("Invalid URL", category="danger")
        return redirect("/")

    return redirect(result[0][0])

  
@app.route("/api", methods=["GET"])
def api():
  
  print(request.args.get("custom"))

  codes = c.execute("SELECT code FROM urls").fetchall()
  
  if not request.args.get("url"):
    return jsonify(error="No enough information provided")
  
  if url_validator(request.args.get("url")) == False:
    return jsonify(error="Not valid URL")
  
  if request.args.get("custom"):
    for code in codes:
      if request.args.get("custom") in codes:
        return jsonify(error="custom code already exist")
  
  code = random_string()
  while (code,) in codes:
    code = random_string()
    
  if request.args.get("custom"):
    c.execute("INSERT INTO urls (auto_code, code, original_url) VALUES (:code, :custom, :url)", {"code": code, "custom": request.args.get("custom"), "url": request.args.get("url")})
    conn.commit()
    return jsonify(newURL=BASE_URL + "/" + request.args.get("custom"))

  
  c.execute("INSERT INTO urls (auto_code, code, original_url) VALUES (:code, :custom, :url)", {"code": code, "custom": code, "url": request.args.get("url")})
  conn.commit()
  return jsonify(newURL=BASE_URL + "/" + code)
  
  
if __name__ == "__main__":
  app.run(debug=True)